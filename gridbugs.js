/**
 * Sky C
 * 190407
 */

/**
 *      FUTURE IDEAS
 *
 *      add sliders for parameters
 *      https://www.w3schools.com/howto/howto_js_rangeslider.asp
 *
 *      abstract tile/bug parameters (use objects with keys)
 */

import {Bug} from './bug.js';
import {Tile} from './tile.js';
import {createSVG, randomName} from './funcs.js';
import {parameters, DOM} from './parameters.js';

class Sim {
    constructor() {
        // Set default sim values
        this.running = true;
        this.fps = parameters.FPS;
        this.animationframes = parameters.animationframes;
        this.frame = 0;

        this.NUM_BUGS = parameters.NUM_BUGS;
        this.CELLSX = parameters.CELLSX;
        this.CELLSY = parameters.CELLSY;
        this.CELLSIZE = parameters.CELLSIZE;

        this.setup();

        // Start running loop
        window.setInterval(() => this.GameLoop(), 1000 / this.fps);

        this.tracked = this.bugs[Object.keys(this.bugs)[0]];
    }

    setup() {
        // Insert svg container
        this.svg = createSVG(
            'left',
            'svg',
            {
                'id': DOM.svgcanvas,
                'width': this.CELLSX * this.CELLSIZE,
                'height': this.CELLSY * this.CELLSIZE
            },
        );

        // Insert control panel
        this.controls = document.createElement('div');
        this.controls.setAttribute('id', DOM.controlsdiv);
        document.getElementById('right').insertAdjacentElement('beforeend', this.controls);

        // Insert speed label
        this.speedspan = document.createElement('span');
        this.speedspan.setAttribute('id', DOM.speedtext);
        this.speedspan.innerHTML = `Animation frames: ${this.animationframes}`;
        this.controls.insertAdjacentElement('beforeend', this.speedspan);

        // Insert slider
        this.speedslider = document.createElement('input');
        this.speedslider.setAttribute('type', 'range');
        this.speedslider.setAttribute('id', DOM.speedslider);
        this.speedslider.setAttribute('min', parameters.min_animationframes);
        this.speedslider.setAttribute('max', parameters.max_animationframes);
        this.speedslider.setAttribute('value', this.animationframes);
        this.controls.insertAdjacentElement('beforeend', this.speedslider);
        this.speedslider.addEventListener('input', () => this.updateSpeed());

        // Insert info container
        this.infospan = document.createElement('span');
        this.infospan.setAttribute('id', DOM.mouseinfotext);
        document.getElementById('right').insertAdjacentElement('beforeend', this.infospan);

        // Insert tracking container
        this.trackspan = document.createElement('span');
        this.trackspan.setAttribute('id', DOM.trackinfotext);
        document.getElementById('right').insertAdjacentElement('beforeend', this.trackspan);

        // Insert summary container
        this.summary = document.createElement('div');
        this.summary.setAttribute('id', DOM.summarydiv);
        document.getElementById('right').insertAdjacentElement('beforeend', this.summary);

        // Insert bug count
        this.summarytext = document.createElement('div');
        this.summarytext.setAttribute('id', DOM.bugcounttext);
        this.summary.insertAdjacentElement('beforeend', this.summarytext);

        // Create grid
        this.grid = {};
        for (var x = 0; x < this.CELLSX; x++) {
            for (var y = 0; y < this.CELLSY; y++) {
                this.grid[[x,y]] = new Tile(this, x, y);
            }
        }

        // Insert tracking cursor
        this.trackcursor = createSVG(
            DOM.svgcanvas,
            'rect',
            {
                'id': 'infocursor',
                'x': 0 * this.CELLSIZE,
                'y': 0 * this.CELLSIZE,
                'height': this.CELLSIZE,
                'width': this.CELLSIZE,
                'stroke': '#FF0000',
                'stroke-width': 2,
                'fill': 'transparent',
                'display': 'none',
            },
            {
                'mouseover': e => this.MouseOverTile(e),
            }
        );

        // Insert info cursor
        this.infocursor = createSVG(
            DOM.svgcanvas,
            'rect',
            {
                'id': 'infocursor',
                'x': 0 * this.CELLSIZE,
                'y': 0 * this.CELLSIZE,
                'height': this.CELLSIZE,
                'width': this.CELLSIZE,
                'stroke': '#000000',
                'stroke-width': 3,
                'fill': 'transparent',
            },
            {
                'click': e => this.ClickTile(this.hovertile),
            }
        );

        // Create bugs
        this.bugs = {};
        for (let i = 0; i < this.NUM_BUGS; i++) {
            const id = randomName(parameters.bug_name_length);

            let x = 0;
            let y = 0;
            while (this.grid[[x, y]].hasOccupant()) {
                x = Math.round(Math.random() * (this.CELLSX - 1));
                y = Math.round(Math.random() * (this.CELLSY - 1));
            }
            const tile = this.grid[[x, y]];

            const dnalength = parameters.bug_dna_length;
            const dna = Array.from({length: dnalength}, () => Math.round(Math.random())).join('');

            this.spawnBug(id, tile, dna)
        }
    }

    spawnBug(id, tile, dna) {
        this.bugs[id] = new Bug(this, id, tile, dna);
    }

    GameLoop() {
        if (this.running) {
            for (let key of Object.keys(this.grid)) {
                const tile = this.grid[key];
                if (this.frame == 0) tile.behavior();
                tile.update(this.frame / this.animationframes);
            }
            for (let key of Object.keys(this.bugs)) {
                const bug = this.bugs[key];
                if (this.frame == 0) bug.behavior();
                bug.update(this.frame / this.animationframes);
            }
            this.update();
            this.frame++;
            if (this.frame >= this.animationframes) this.frame = 0;
        }
    }

    update() {
        // Show hover cursor
        if (this.hovertile) {
            this.ShowTileInfo(this.hovertile);
            this.infocursor.setAttributeNS(null, 'x', this.hovertile.x * this.CELLSIZE);
            this.infocursor.setAttributeNS(null, 'y', this.hovertile.y * this.CELLSIZE);

            // const tiles = this.grid[[this.hovertile.x, this.hovertile.y]].getNearbyTiles(5, 'real');
            // for (let tile of tiles) {
            //     tile.setSVGProperties({'fill':'#FFFFFF'})
            // }
        }
        // Show tracked cursor
        if (this.tracked) {
            this.ShowBugInfo(this.tracked);
            this.trackcursor.setAttributeNS(null, 'x', this.tracked.tile.x * this.CELLSIZE);
            this.trackcursor.setAttributeNS(null, 'y', this.tracked.tile.y * this.CELLSIZE);
            this.trackcursor.setAttributeNS(null, 'display', 'block');
        } else {
            this.trackcursor.setAttributeNS(null, 'display', 'none');
        }
        // Update summary
        this.ShowSummaryInfo();
    }

    MouseOverTile(e, verbose=false) {
        if (verbose) console.log('MouseOverTile', e);
        if (e.path[0] == this.trackcursor) {
            this.hovertile = this.tracked.tile;
        } else if (e.path[0] != this.infocursor) {
            const cell = e.path[0];
            const y = cell.getAttribute('row');
            const x = cell.getAttribute('column');
            const tile = this.grid[[x, y]];
            this.hovertile = tile;
        }
        this.ShowTileInfo(this.hovertile);
    }

    ClickTile(target, verbose=false) {
        if (verbose) console.log('ClickTile', e);
        if (target instanceof Bug) {
            this.tracked = target;
        } else if (target instanceof Tile) {
            const tile = this.grid[[this.hovertile.x, this.hovertile.y]];
            if (tile.hasOccupant()) {
                this.tracked = tile.occupant;
            } else {
                this.tracked = undefined;
                // make it easier to click on fast-moving bugs
                for (let bugid of Object.keys(this.bugs)) {
                    const bug = this.bugs[bugid];
                    if (bug.last.tile == tile) {
                        this.tracked = bug;
                    }
                }
            }
        }
    }

    updateSpeed() {
        this.animationframes = this.speedslider.value;
        this.speedspan.innerHTML = `Animation frames: ${this.animationframes}`;
    }

    ShowTileInfo(tile) {
        let str = '';
        str += `
        x: ${tile.x}<br>
        y: ${tile.y}<br>
        grass: ${Math.round(tile.grass)}<br>
        `;
        if (tile.hasOccupant()) {
            str += `bug: ${tile.occupant.name}`;
        }
        document.getElementById(DOM.mouseinfotext).innerHTML = str;
    }

    ShowBugInfo(bug) {
        let str = '';
        str += `
        name:${bug.name}<br>
        x: ${bug.tile.x}<br>
        y: ${bug.tile.y}<br>
        food: ${bug.food}<br>
        thoughts: ${bug.thoughts}<br>
        `;
        document.getElementById(DOM.trackinfotext).innerHTML = str;
    }

    ShowSummaryInfo() {
        const numbugs = Object.keys(this.bugs).length;
        let totalgrass = 0;
        for (let tileind of Object.keys(this.grid)) {
            const tile = this.grid[tileind];
            totalgrass += tile.grass;
        }

        let summarytext = ``;
        summarytext += `${numbugs} bugs<br>`
        summarytext += `${totalgrass} grass (+ ${Object.keys(this.grid).length * parameters.tile_grass_growth_chance * parameters.tile_grass_growth_amount} - ${numbugs * parameters.bug_metabolism})<br>`
        summarytext += `<table><tr><th>name</th><th>food</th></tr>`;
        for (let bugname of Object.keys(this.bugs)) {
            const bug = this.bugs[bugname];
            summarytext += `<tr><td>${bug.name}</td><td>${bug.food}</td></tr>`;

        }
        summarytext += `</table>`;
        this.summarytext.innerHTML = summarytext;
    }
}

let SIM = new Sim();