To run statically, open *gridbugsone.html* in a browser.

To run dynamically, use a local hosting tool (like https://www.npmjs.com/package/live-server) and open *gridbugs.html*.