import {createSVG, copyObject, interpolate, randomColor, zeroPad} from './funcs.js';
import {parameters, DOM} from './parameters.js';

export class Tile {
    constructor(sim, x, y) {
        this.sim = sim;
        this.x = x;
        this.y = y;
        this.loc = [x,y];
        this.occupant = undefined;
        this.grass = Math.round(Math.random() * 99);
        this.r = Math.round(Math.random() * 255);
        this.g = Math.round(Math.random() * 255);
        this.b = Math.round(Math.random() * 255);

        this.svg = createSVG(
            DOM.svgcanvas,
            'rect',
            {
                'x': x * this.sim.CELLSIZE,
                'y': y * this.sim.CELLSIZE,
                'class': 'cell column-' + x.toString() + ' row-' + y.toString(),
                'id': x.toString() + ',' + y.toString(),
                'column': x,
                'row': y,
                'height': this.sim.CELLSIZE,
                'width': this.sim.CELLSIZE,
                'stroke': '#000000',
                'fill': '#FFFFFF',
            },
            {
                'mouseover': e => sim.MouseOverTile(e),
            },
        );
    }

    setSVGProperty(property, value) {
        this.svg.setAttributeNS(null, property, value);
    }

    getSVGProperty(property) {
        return this.svg.getAttribute(property);
    }

    behavior() {
        this.last = copyObject(this);

        if (Math.random() > parameters.tile_grass_growth_chance) this.grass = Math.min(this.grass + parameters.tile_grass_growth_amount, parameters.tile_grass_max);

        // const colorindex = Math.floor(Math.random() * 3);
        // if (colorindex == 0) {
        //     this.r = Math.min(this.r + 1, 255);
        // } else if (colorindex == 1) {
        //     this.g = Math.min(this.g + 1, 255);
        // } else {
        //     this.b = Math.min(this.b + 1, 255);
        // }
    }

    update(step) {
        const animgrass = interpolate(step, this.last.grass, this.grass);
        this.setSVGProperty('fill', '#00' + Math.round(200 - animgrass, 0).toString(16) + '00');

        // const r = interpolate(step, this.last.r, this.r);
        // const g = interpolate(step, this.last.g, this.g);
        // const b = interpolate(step, this.last.b, this.b);
        // this.setSVGProperty('fill', '#' + zeroPad(Math.round(r).toString(16),2) + zeroPad(Math.round(g).toString(16),2) + zeroPad(Math.round(b).toString(16),2));
    }

    hasOccupant() {
        if (this.occupant) return true;
        return false;
    }

    getOffsetTile(dx, dy) {
        const x = this.x + dx;
        const y = this.y + dy;
        if (x >=0 && x < this.sim.CELLSX && y >=0 && y < this.sim.CELLSY) {
            return this.sim.grid[[x, y]];
        } else {
            return false;
        }
    }

    getNearbyTiles(radius, style) {
        let cells = [];
        if (style == 'square') {
            for (let x = this.x - radius; x <= this.x + radius; x++) {
                if (x >=0 && x < this.sim.CELLSX) {
                    for (let y = this.y - radius; y <= this.y + radius; y++) {
                        if (y >=0 && y < this.sim.CELLSY) {
                            cells.push(this.sim.grid[[x,y]])
                        }
                    }
                }
            }
        } else if (style == 'manhattan') {
            for (let x = this.x - radius; x <= this.x + radius; x++) {
                if (x >=0 && x < this.sim.CELLSX) {
                    for (let y = this.y - radius; y <= this.y + radius; y++) {
                        if (y >=0 && y < this.sim.CELLSY) {
                            if (Math.abs(this.x - x) + Math.abs(this.y - y) <= radius) {
                                cells.push(this.sim.grid[[x,y]])
                            }
                        }
                    }
                }
            }
        } else if (style == 'real') {
            for (let x = this.x - radius; x <= this.x + radius; x++) {
                if (x >=0 && x < this.sim.CELLSX) {
                    for (let y = this.y - radius; y <= this.y + radius; y++) {
                        if (y >=0 && y < this.sim.CELLSY) {
                            if ((this.x - x) ** 2 + (this.y - y) ** 2 <= radius ** 2) {
                                cells.push(this.sim.grid[[x,y]])
                            }
                        }
                    }
                }
            }
        }
        return cells;
    }
}