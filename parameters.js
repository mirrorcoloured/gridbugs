export const parameters = {
    FPS : 60,
    animationframes : 15,
    min_animationframes : 1,
    max_animationframes : 120,

    NUM_BUGS : 10,
    CELLSX : 20,
    CELLSY : 20,
    CELLSIZE : 40,

    bug_name_length : 6,
    bug_dna_length : 120,
    bug_starting_food : 50,
    bug_bitesize : 20,
    bug_metabolism : 15,
    bug_childcost : 100,
    bug_childthreshold : 200,
    bug_maxfood : 200,
    bug_sight_radius : 5,
    bug_view : 'square',
    bug_mutation_chance : 0.001,

    tile_grass_growth_chance : 0.5,
    tile_grass_growth_amount : 1,
    tile_grass_max : 99,
}

export const DOM = {
    svgcanvas : 'svgcanvas',
    controlsdiv : 'controls',
    speedtext : 'speedspan',
    speedslider : 'speedslider',
    mouseinfotext : 'mouseinfospan',
    trackinfotext : 'trackinfospan',
    summarydiv : 'summary',
    summarytext : 'summarytext',
}