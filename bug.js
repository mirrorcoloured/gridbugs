import {createSVG, createGradient, minmax, getAngle, shuffle, copyObject, randomName, mutateDNA, HexStringToRGB, colorDifference, zeroPad} from './funcs.js';
import {parameters, DOM} from './parameters.js';

export class Bug {
    constructor(sim, id, tile, dna) {
        this.sim = sim;
        this.name = id;
        this.dna = dna;
        this.dnalength = dna.length;
        this.tile = tile;
        this.tile.occupant = this;
        this.thoughts = '';

        this.food = parameters.bug_starting_food;
        this.bitesize = parameters.bug_bitesize;
        this.metabolism = parameters.bug_metabolism;
        this.childcost = parameters.bug_childcost;
        this.childthreshold = parameters.bug_childthreshold;

        this.favoritecolor = '#' + zeroPad((parseInt(this.dna, 2) % 0xFFFFFF).toString(16),6);

        this.svg = createSVG(
            DOM.svgcanvas,
            'circle',
            {
                'cx': (this.tile.x + 0.5) * this.sim.CELLSIZE,
                'cy': (this.tile.y + 0.5) * this.sim.CELLSIZE,
                'id': this.name,
                'r': this.sim.CELLSIZE * 0.25,
                'stroke': '#000000',
                'fill': `url('#gradient-${this.name}')`,
                // 'fill': `${this.favoritecolor}`,
                'class': 'bug',
            },
            {
                'click': e => sim.ClickTile(this),
            }
        );

        const parts = this.splitdna(3);
        const rotate = new Date().getMilliseconds() % 90;

        const g = createGradient(
            document.getElementById(DOM.svgcanvas),
            'gradient-' + this.name,
            [
                {offset:'33%', 'stop-color':'#' + (parseInt(parts[0], 2) % 0xFFFFFF).toString(16)},
                {offset:'66%', 'stop-color':'#' + (parseInt(parts[1], 2) % 0xFFFFFF).toString(16)},
                {offset:'100%','stop-color':'#' + (parseInt(parts[2], 2) % 0xFFFFFF).toString(16)}
            ],
            `rotate(${rotate})`
        )

        this.last = this;
    }

    die() {
        document.getElementById(DOM.svgcanvas).removeChild(this.svg);
        this.tile.occupant = undefined;
        delete this.sim.bugs[this.name]
    }

    /**
     * Divides this.dna into equal length parts (uneven division results in the last part being longer)
     * @param {int} [num_parts]
     * @param {int} [length]
     */
    splitdna(num_parts, length) {
        let parts = [];

        if (length == undefined) {
            length = Math.floor(this.dnalength / num_parts);
        } else if (num_parts == undefined) {
            num_parts = Math.floor(this.dnalength / length);
        } else {
            length = 1;
            num_parts = Math.floor(this.dnalength / length);
        }

        for (let i = 0; i < num_parts; i++) {
            let part = this.dna.slice(i * length, (i + 1) * length);
            if (i == num_parts - 1) {
                part += this.dna.slice((i + 1) * length, (i + 2) * length);
            }
            parts.push(part);
        }
        return parts;
    }

    sense(radius) {
        return this.tile.getNearbyTiles(radius, parameters.bug_view);
    }

    moveToTile(tile) {
        if (tile.hasOccupant() == false) {
            this.tile.occupant = undefined;
            this.tile = tile;
            tile.occupant = this;
        }
    }

    moveTowardTile(tile) {
        if (this.tile != tile) {
            const theta = getAngle(this.tile.x, this.tile.y, tile.x, tile.y);
            this.moveInDirection(theta, true);
        }
    }

    moveInDirection(theta, diagonal=true) {
        let ind;
        let discrete;
        let offsets;
        if (diagonal) {
            ind = (Math.floor((theta - (Math.PI / 8)) / (Math.PI / 4)) + 8) % 8;
            discrete = [[1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1], [1, -1], [1, 0]];
            offsets = [0, 1, -1, 2, -2];
        } else {
            ind = (Math.floor((theta - (Math.PI / 4)) / (Math.PI / 2)) + 4) % 4;
            discrete = [[0, 1], [-1, 0], [0, -1], [1, 0]];
            offsets = [0, 1, -1];
        }
        for (let offset of offsets) {
            const tile = this.tile.getOffsetTile(...discrete[(ind + offset + discrete.length) % discrete.length]);
            if (tile) {
                if (tile.hasOccupant() == false) {
                    this.moveToTile(tile);
                    return true;
                }
            }
        }
        return false;
    }

    getCentroidTile(tiles, property) {
        let centx = 0;
        let centy = 0;
        let sum = 0;
        for (let tile of tiles) {
            sum += tile[property];
            centx += tile[property] * tile.x;
            centy += tile[property] * tile.y;
        }
        return this.sim.grid[[Math.round(centx /= sum,0), Math.round(centy /= sum,0)]]
    }

    asexualReproduce() {
        const name = randomName(8);

        let tiles = this.tile.getNearbyTiles(1, 'square');
        tiles.filter((tile) => tile.hasOccupant() == false);
        shuffle(tiles);
        const tile = tiles[0];

        const dna = mutateDNA(this.dna, parameters.bug_mutation_chance);

        this.sim.spawnBug(name, tile, dna);
    }

    compareDNA(otherdna) {
        let difference = 0;
        for (let i = 0; i < this.dnalength; i++) {
            if (this.dna[i] != otherdna[i]) difference += 1;
        }
        return difference;
    }

    paintFloor(color) {
        const rgb = HexStringToRGB(color);
        this.tile.r = rgb[0];
        this.tile.g = rgb[1];
        this.tile.b = rgb[2];
    }

    behavior() {
        this.last = copyObject(this);

        // SENSE

        // let targettile;
        // if (this.sim.lasthover) {
        //     targettile = this.sim.lasthover;
        // }

        // let alltiles = [];
        // for (let x = 0; x < this.sim.CELLSX; x++) {
        //     for (let y = 0; y < this.sim.CELLSY; y++) {
        //         alltiles.push(this.sim.grid[[x, y]]);
        //     }
        // }
        // const targettile = this.getCentroidTile(alltiles, 'grass');

        let tiles = this.sense(parameters.bug_sight_radius);
        shuffle(tiles);
        tiles.filter((tile) => tile.hasOccupant() == false);
        tiles.sort((a,b) => b.grass - a.grass)
        // tiles.sort((a,b) => colorDifference(b.getSVGProperty('fill'), this.favoritecolor, false) - colorDifference(a.getSVGProperty('fill'), this.favoritecolor, false))

        const targettile = tiles[0];

        // MOVE

        this.moveTowardTile(targettile);
        this.thoughts = 'moving';

        // ACT

        // const diff = colorDifference(this.tile.getSVGProperty('fill'), this.favoritecolor, false);
        // if (diff > 0) {
        //     this.paintFloor(this.favoritecolor)
        // }

        // this.food = this.food + Math.round(diff / (255 * 3) * 50) - 5;

        if (this.tile.grass > this.bitesize) {
            this.tile.grass -= this.bitesize;
            this.food += this.bitesize;
            this.thoughts = 'eating';
        } else {
            this.thoughts = 'no food here'
        }

        this.food -= this.metabolism;

        if (this.food > this.childthreshold) {
            this.food -= this.childcost;
            this.asexualReproduce();
            this.thoughts = 'having a baby';
        }

        this.food = Math.min(this.food, parameters.bug_maxfood);

        // END

        if (this.food <= 0) {
            this.thoughts = 'dying';
            this.die();
        }
    }

    randomWalk() {
        const movex = Math.round(Math.random() * 2) - 1;
        const movey = Math.round(Math.random() * 2) - 1;
        const targetx = minmax(this.tile.x + movex, 0, this.sim.CELLSX - 1);
        const targety = minmax(this.tile.y + movey, 0, this.sim.CELLSY - 1);
        this.moveToTile(this.sim.grid[[targetx, targety]]);
    }

    update(step) {
        // calculate progress along path
        const drawx = step * this.tile.x + (1 - step) * this.last.tile.x;
        const drawy = step * this.tile.y + (1 - step) * this.last.tile.y;
        // update svg location
        this.svg.setAttributeNS(null, 'cx', (drawx + 0.5) * this.sim.CELLSIZE);
        this.svg.setAttributeNS(null, 'cy', (drawy + 0.5) * this.sim.CELLSIZE);
    }
}