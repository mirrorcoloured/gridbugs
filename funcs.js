export function mutateDNA(dna, errorchance) {
    let newdna = '';
    for (let i = 0; i < dna.length; i++) {
        const chance = Math.random();
        if (chance < errorchance) {
            newdna += (dna[i] == '1' ? '0' : '1');
        } else {
            newdna += dna[i];
        }
    }
    return newdna;
}

export function createSVG(parentid, qualifiedName, properties, eventListeners) {
    let svg = document.createElementNS('http://www.w3.org/2000/svg', qualifiedName);
    if (properties) {
        for (let key of Object.keys(properties)) {
            svg.setAttributeNS(null, key, properties[key]);
        }
    }
    if (eventListeners) {
        for (let key of Object.keys(eventListeners)) {
            svg.addEventListener(key, eventListeners[key]);
        }
    }
    document.getElementById(parentid).appendChild(svg);
    return svg;
}

// svg:   the owning <svg> element
// id:    an id='...' attribute for the gradient
// stops: an array of objects with <stop> attributes
export function createGradient(svg, id, stops, transform=''){
    var svgNS = svg.namespaceURI;
    var grad  = document.createElementNS(svgNS,'linearGradient');
    grad.setAttribute('id', id);
    grad.setAttribute('gradientTransform', transform)
    for (var i=0;i<stops.length;i++){
        var attrs = stops[i];
        var stop = document.createElementNS(svgNS,'stop');
        for (var attr in attrs){
            if (attrs.hasOwnProperty(attr)) stop.setAttribute(attr,attrs[attr]);
        }
        grad.appendChild(stop);
    }

    var defs = svg.querySelector('defs') ||
        svg.insertBefore( document.createElementNS(svgNS,'defs'), svg.firstChild);
    return defs.appendChild(grad);
}

export function minmax(num, min, max) {
    return Math.max(Math.min(num, max), min);
}

export function zeroPad(num, width, char = '0') {
    num = String(num);
    return num.length >= width ? num : new Array(width - num.length + 1).join(char) + num;
}

export function randomColor() {
    return Math.round(0xffffff * Math.random()).toString(16);
}

export function randomName(length) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    let name = '';
    while (name.length < length) {
        name += alphabet[Math.round(Math.random() * 25)];
    }
    return name;
}

export function getAngle(x1, y1, x2, y2) {
    const dx = x2 - x1;
    const dy = y2 - y1;
    if (dx >= 0) {
        return Math.atan(dy / dx);
    } else if (dx < 0) {
        return Math.atan(dy / dx) + Math.PI;
    }
}

export function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
        [array[i], array[j]] = [array[j], array[i]]; // swap elements
    }
}

export function copyObject(object) {
    let obj = {};
    for (let key of Object.keys(object)) {
        obj[key] = object[key];
    }
    return obj;
}

export function interpolate(fraction, oldval, newval) {
    return fraction * (newval - oldval) + oldval;
}

export function colorDifference(color1, color2, absolute=true) {
    let rgb1 = HexStringToRGB(color1);
    let rgb2 = HexStringToRGB(color2);
    if (absolute) {
        return Math.abs(rgb1[0]-rgb2[0]) + Math.abs(rgb1[1]-rgb2[1]) + Math.abs(rgb1[2]-rgb2[2]);
    } else {
        return rgb1[0]-rgb2[0] + rgb1[1]-rgb2[1] + rgb1[2]-rgb2[2];
    }
}

export function RGBToHexString(r, g, b) {
    return `#${zeroPad((r % 0xFFFFFF).toString(16),2)}${zeroPad((g % 0xFFFFFF).toString(16),2)}${zeroPad((b % 0xFFFFFF).toString(16),2)}`;
}

export function HexStringToRGB(color) {
    const r = parseInt(color.slice(1, 3), 16);
    const g = parseInt(color.slice(3, 5), 16);
    const b = parseInt(color.slice(5, 7), 16);
    return [r, g, b];
}